/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XVBAToOOEventDescGen.idl,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __ooo_vba_XVBAToOOEventDescGen_idl__
#define __ooo_vba_XVBAToOOEventDescGen_idl__

#ifndef __com_sun_star_script_ScriptEventDescriptor_idl__
#include <com/sun/star/script/ScriptEventDescriptor.idl>
#endif 

#ifndef __com_sun_star_script_XScriptEventsSupplier_idl__
#include <com/sun/star/script/XScriptEventsSupplier.idl>
#endif 

module ooo {  module vba { 
interface XVBAToOOEventDescGen : com::sun::star::uno::XInterface
{
	sequence< com::sun::star::script::ScriptEventDescriptor >  getEventDescriptions( [in] com::sun::star::uno::XInterface xControl, [in] string sCodeName );
	com::sun::star::script::XScriptEventsSupplier getEventSupplier( [in] com::sun::star::uno::XInterface xControl, [in] string sCodeName );
};

}; };
#endif

/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XWorksheet.idl,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __ooo_vba_excel_XWorksheet_idl__
#define __ooo_vba_excel_XWorksheet_idl__

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif

#ifndef __ooo_vba_XHelperInterface_idl__
#include <ooo/vba/XHelperInterface.idl>
#endif

#ifndef __com_sun_star_script_XInvocation_idl__
#include <com/sun/star/script/XInvocation.idl>
#endif

#ifndef __com_sun_star_container_XNamed_idl__
#include <com/sun/star/container/XNamed.idl>
#endif
//=============================================================================

module ooo {  module vba {  module excel { 

//=============================================================================

interface XComments;
interface XRange;
interface XOutline;
interface XPageSetup;
interface XHPageBreaks;
interface XWorksheet
{
	interface ::ooo::vba::XHelperInterface;
	interface ::com::sun::star::script::XInvocation;
	interface ::com::sun::star::container::XNamed;

	[attribute] boolean Visible; 
	[attribute, readonly] long StandardHeight;
	[attribute, readonly] long StandardWidth;
	[attribute, readonly] boolean ProtectionMode;
	[attribute, readonly] boolean ProtectContents;
	[attribute, readonly] boolean ProtectDrawingObjects;
	[attribute, readonly] XRange UsedRange; 
	[attribute, readonly] XWorksheet Next;
	[attribute, readonly] XWorksheet Previous;
	[attribute, readonly] string CodeName;
 	[attribute, readonly] short Index;
 	[attribute] long EnableSelection;
	
	void Activate();
	void Calculate( );
	void Select();
	void Move([in] any Before,[in] any After );
	void Copy([in] any Before,[in] any After );
	void Paste([in] any Destination,[in] any Link);
	void Delete( );
	void Protect([in] any Password,[in] any DrawingObjects ,[in] any Contents,[in] any Scenarios,[in] any UserInterfaceOnly);
	void Unprotect([in] any Password );
	void CheckSpelling([in] any CustomDictionary,[in] any IgnoreUppercase, [in] any AlwaysSuggest,[in] any SpellingLang );
	XRange Range([in] any Cell1, [in] any Cell2 );
	any ChartObjects([in] any Index);
	any PivotTables([in] any Index);
	any Comments([in] any Index);
	XOutline Outline();
	XPageSetup PageSetup();
	any HPageBreaks([in] any Index);
	any OLEObjects([in] any Index);
	void ShowDataForm();
	any Shapes([in] any Index);


// FIXME: should prolly inherit from Range somehow...
	XRange Cells([in] any RowIndex, [in] any ColumnIndex);
	XRange Rows([in] any aIndex);
	XRange Columns([in] any aIndex);

	any Evaluate( [in] string Name);
	

	void setEnableCalculation([in] boolean EnableCalculation) raises(com::sun::star::script::BasicErrorException);
	boolean getEnableCalculation() raises(com::sun::star::script::BasicErrorException);
	void PrintOut( [in] any From, [in] any To, [in] any Copies, [in] any Preview, [in] any ActivePrinter, [in] any PrintToFile, [in] any Collate, [in] any PrToFileName, [in] any IgnorePrintAreas );


};

//=============================================================================

}; }; };

#endif



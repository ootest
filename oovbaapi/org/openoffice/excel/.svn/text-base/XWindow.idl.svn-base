/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XWindow.idl,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __org_openoffice_excel_XWindow_idl__
#define __org_openoffice_excel_XWindow_idl__

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif

#ifndef __org_openoffice_vba_XHelperInterface_idl__
#include <org/openoffice/vba/XHelperInterface.idl>
#endif

//=============================================================================

module org {  module openoffice {  module excel { 

//=============================================================================
interface XRange;
interface XWindow
{
	interface ::org::openoffice::vba::XHelperInterface;

	[attribute] any Caption;
	[attribute] boolean DisplayGridlines; 
	[attribute] boolean DisplayHeadings; 
	[attribute] boolean DisplayHorizontalScrollBar; 
	[attribute] boolean DisplayOutline;
	[attribute] boolean DisplayVerticalScrollBar; 
	[attribute] boolean DisplayWorkbookTabs; 
	[attribute] boolean FreezePanes; 
	[attribute] boolean Split; 
	[attribute] long SplitColumn;
	[attribute] double SplitHorizontal;
	[attribute] long SplitRow;
	[attribute] double SplitVertical;
	[attribute] any ScrollColumn;
	[attribute] any ScrollRow;
	[attribute] any WindowState;
	[attribute] any Zoom;
	any SelectedSheets( [in] any aIndex );
	void SmallScroll( [in] any Down, [in] any Up, [in] any ToRight, [in] any ToLeft );
	void LargeScroll( [in] any Down, [in] any Up, [in] any ToRight, [in] any ToLeft );
	void ScrollWorkbookTabs( [in] any Sheets, [in] any Position );
	void Activate();
        void Close([in] any SaveChanges, [in] any FileName, [in] any RouteWorkBook);
	XRange ActiveCell() raises(com::sun::star::script::BasicErrorException);
	any Selection() raises(com::sun::star::script::BasicErrorException);

};

//=============================================================================

}; }; };

#endif



#ifndef INCLUDED_OOO_VBA_GLOBALS_HPP
#define INCLUDED_OOO_VBA_GLOBALS_HPP

#include "sal/config.h"

#include "cppu/unotype.hxx"

namespace ooo { namespace vba {

class Globals {
private:
    Globals(); // not implemented
    Globals(Globals &); // not implemented
    ~Globals(); // not implemented
    void operator =(Globals); // not implemented
};

} }

#endif // INCLUDED_OOO_VBA_GLOBALS_HPP

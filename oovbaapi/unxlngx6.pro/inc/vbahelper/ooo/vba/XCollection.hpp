#ifndef INCLUDED_OOO_VBA_XCOLLECTION_HPP
#define INCLUDED_OOO_VBA_XCOLLECTION_HPP

#include "sal/config.h"

#include "ooo/vba/XCollection.hdl"

#include "ooo/vba/XHelperInterface.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/script/XDefaultMethod.hpp"
#include "com/sun/star/container/XEnumerationAccess.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "sal/types.h"

namespace ooo { namespace vba {

inline ::com::sun::star::uno::Type const & cppu_detail_getUnoType(::ooo::vba::XCollection const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_TypeDescriptionReference * aSuperTypes[3];
        aSuperTypes[0] = ::cppu::UnoType< const ::com::sun::star::uno::Reference< ::ooo::vba::XHelperInterface > >::get().getTypeLibType();
        aSuperTypes[1] = ::cppu::UnoType< const ::com::sun::star::uno::Reference< ::com::sun::star::script::XDefaultMethod > >::get().getTypeLibType();
        aSuperTypes[2] = ::cppu::UnoType< const ::com::sun::star::uno::Reference< ::com::sun::star::container::XEnumerationAccess > >::get().getTypeLibType();
        typelib_static_mi_interface_type_init( &the_type, "ooo.vba.XCollection", 3, aSuperTypes );
    }
    return * reinterpret_cast< ::com::sun::star::uno::Type * >( &the_type );
}

} }

inline ::com::sun::star::uno::Type const & SAL_CALL getCppuType(::com::sun::star::uno::Reference< ::ooo::vba::XCollection > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::com::sun::star::uno::Reference< ::ooo::vba::XCollection > >::get();
}

::com::sun::star::uno::Type const & ::ooo::vba::XCollection::static_type(void *) {
    return ::getCppuType(static_cast< ::com::sun::star::uno::Reference< ::ooo::vba::XCollection > * >(0));
}

#endif // INCLUDED_OOO_VBA_XCOLLECTION_HPP

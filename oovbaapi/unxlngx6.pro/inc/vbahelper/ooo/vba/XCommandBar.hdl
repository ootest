#ifndef INCLUDED_OOO_VBA_XCOMMANDBAR_HDL
#define INCLUDED_OOO_VBA_XCOMMANDBAR_HDL

#include "sal/config.h"

#include "ooo/vba/XHelperInterface.hdl"
#include "com/sun/star/script/BasicErrorException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno {
class Type;
} } } }

namespace ooo { namespace vba {

class SAL_NO_VTABLE XCommandBar : public ::ooo::vba::XHelperInterface
{
public:

    // Attributes
    virtual ::rtl::OUString SAL_CALL getName() throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual void SAL_CALL setName( const ::rtl::OUString& _name ) throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual ::sal_Bool SAL_CALL getVisible() throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual void SAL_CALL setVisible( ::sal_Bool _visible ) throw (::com::sun::star::uno::RuntimeException) = 0;

    // Methods
    virtual void SAL_CALL Delete(  ) throw (::com::sun::star::script::BasicErrorException, ::com::sun::star::uno::RuntimeException) = 0;
    virtual ::com::sun::star::uno::Any SAL_CALL Controls( const ::com::sun::star::uno::Any& Index ) throw (::com::sun::star::script::BasicErrorException, ::com::sun::star::uno::RuntimeException) = 0;
    virtual ::sal_Int32 SAL_CALL Type(  ) throw (::com::sun::star::script::BasicErrorException, ::com::sun::star::uno::RuntimeException) = 0;
    virtual ::com::sun::star::uno::Any SAL_CALL FindControl( const ::com::sun::star::uno::Any& Type, const ::com::sun::star::uno::Any& Id, const ::com::sun::star::uno::Any& Tag, const ::com::sun::star::uno::Any& Visible, const ::com::sun::star::uno::Any& Recursive ) throw (::com::sun::star::script::BasicErrorException, ::com::sun::star::uno::RuntimeException) = 0;

    static inline ::com::sun::star::uno::Type const & SAL_CALL static_type(void * = 0);
};

} }

inline const ::com::sun::star::uno::Type& SAL_CALL getCppuType( const ::com::sun::star::uno::Reference< ::ooo::vba::XCommandBar >* ) SAL_THROW( () );

#endif // INCLUDED_OOO_VBA_XCOMMANDBAR_HDL

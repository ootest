#ifndef INCLUDED_OOO_VBA_XCOMMANDBARBUTTON_HPP
#define INCLUDED_OOO_VBA_XCOMMANDBARBUTTON_HPP

#include "sal/config.h"

#include "ooo/vba/XCommandBarButton.hdl"

#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"

namespace ooo { namespace vba {

inline ::com::sun::star::uno::Type const & cppu_detail_getUnoType(::ooo::vba::XCommandBarButton const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_mi_interface_type_init( &the_type, "ooo.vba.XCommandBarButton", 0, 0 );
    }
    return * reinterpret_cast< ::com::sun::star::uno::Type * >( &the_type );
}

} }

inline ::com::sun::star::uno::Type const & SAL_CALL getCppuType(::com::sun::star::uno::Reference< ::ooo::vba::XCommandBarButton > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::com::sun::star::uno::Reference< ::ooo::vba::XCommandBarButton > >::get();
}

::com::sun::star::uno::Type const & ::ooo::vba::XCommandBarButton::static_type(void *) {
    return ::getCppuType(static_cast< ::com::sun::star::uno::Reference< ::ooo::vba::XCommandBarButton > * >(0));
}

#endif // INCLUDED_OOO_VBA_XCOMMANDBARBUTTON_HPP

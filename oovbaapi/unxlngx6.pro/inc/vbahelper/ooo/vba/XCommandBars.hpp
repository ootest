#ifndef INCLUDED_OOO_VBA_XCOMMANDBARS_HPP
#define INCLUDED_OOO_VBA_XCOMMANDBARS_HPP

#include "sal/config.h"

#include "ooo/vba/XCommandBars.hdl"

#include "ooo/vba/XCommandBar.hpp"
#include "com/sun/star/script/BasicErrorException.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "ooo/vba/XCollection.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"

namespace ooo { namespace vba {

inline ::com::sun::star::uno::Type const & cppu_detail_getUnoType(::ooo::vba::XCommandBars const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_TypeDescriptionReference * aSuperTypes[1];
        aSuperTypes[0] = ::cppu::UnoType< const ::com::sun::star::uno::Reference< ::ooo::vba::XCollection > >::get().getTypeLibType();
        typelib_static_mi_interface_type_init( &the_type, "ooo.vba.XCommandBars", 1, aSuperTypes );
    }
    return * reinterpret_cast< ::com::sun::star::uno::Type * >( &the_type );
}

} }

inline ::com::sun::star::uno::Type const & SAL_CALL getCppuType(::com::sun::star::uno::Reference< ::ooo::vba::XCommandBars > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::com::sun::star::uno::Reference< ::ooo::vba::XCommandBars > >::get();
}

::com::sun::star::uno::Type const & ::ooo::vba::XCommandBars::static_type(void *) {
    return ::getCppuType(static_cast< ::com::sun::star::uno::Reference< ::ooo::vba::XCommandBars > * >(0));
}

#endif // INCLUDED_OOO_VBA_XCOMMANDBARS_HPP

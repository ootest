#ifndef INCLUDED_OOO_VBA_XGLOBALS_HPP
#define INCLUDED_OOO_VBA_XGLOBALS_HPP

#include "sal/config.h"

#include "ooo/vba/XGlobals.hdl"

#include "ooo/vba/excel/XWorkbook.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "ooo/vba/excel/XWorksheet.hpp"
#include "ooo/vba/excel/XApplication.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"

namespace ooo { namespace vba {

inline ::com::sun::star::uno::Type const & cppu_detail_getUnoType(::ooo::vba::XGlobals const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_mi_interface_type_init( &the_type, "ooo.vba.XGlobals", 0, 0 );
    }
    return * reinterpret_cast< ::com::sun::star::uno::Type * >( &the_type );
}

} }

inline ::com::sun::star::uno::Type const & SAL_CALL getCppuType(::com::sun::star::uno::Reference< ::ooo::vba::XGlobals > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::com::sun::star::uno::Reference< ::ooo::vba::XGlobals > >::get();
}

::com::sun::star::uno::Type const & ::ooo::vba::XGlobals::static_type(void *) {
    return ::getCppuType(static_cast< ::com::sun::star::uno::Reference< ::ooo::vba::XGlobals > * >(0));
}

#endif // INCLUDED_OOO_VBA_XGLOBALS_HPP

#ifndef INCLUDED_OOO_VBA_EXCEL_WINDOW_HPP
#define INCLUDED_OOO_VBA_EXCEL_WINDOW_HPP

#include "sal/config.h"

#include "ooo/vba/XHelperInterface.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/lang/XMultiComponentFactory.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "ooo/vba/excel/XWindow.hpp"
#include "com/sun/star/frame/XModel.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace ooo { namespace vba { namespace excel {

class Window {
public:
    static ::com::sun::star::uno::Reference< ::ooo::vba::excel::XWindow > createWindow(::com::sun::star::uno::Reference< ::com::sun::star::uno::XComponentContext > const & the_context, const ::com::sun::star::uno::Reference< ::ooo::vba::XHelperInterface >& Parent, const ::com::sun::star::uno::Reference< ::com::sun::star::frame::XModel >& xModel) {
        ::com::sun::star::uno::Reference< ::com::sun::star::lang::XMultiComponentFactory > the_factory(the_context->getServiceManager());
        if (!the_factory.is()) {
            throw com::sun::star::uno::DeploymentException(::rtl::OUString(RTL_CONSTASCII_USTRINGPARAM("component context fails to supply service manager")), the_context);
        }
        ::com::sun::star::uno::Sequence< ::com::sun::star::uno::Any > the_arguments(2);
        the_arguments[0] <<= Parent;
        the_arguments[1] <<= xModel;
        ::com::sun::star::uno::Reference< ::ooo::vba::excel::XWindow > the_instance;
        try {
            the_instance = ::com::sun::star::uno::Reference< ::ooo::vba::excel::XWindow >(the_factory->createInstanceWithArgumentsAndContext(::rtl::OUString(RTL_CONSTASCII_USTRINGPARAM("ooo.vba.excel.Window")), the_arguments, the_context), ::com::sun::star::uno::UNO_QUERY);
        } catch (::com::sun::star::uno::RuntimeException &) {
            throw;
        } catch (::com::sun::star::uno::Exception & the_exception) {
            throw ::com::sun::star::uno::DeploymentException(::rtl::OUString(RTL_CONSTASCII_USTRINGPARAM("component context fails to supply service ooo.vba.excel.Window of type ooo.vba.excel.XWindow: ")) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::com::sun::star::uno::DeploymentException(::rtl::OUString(RTL_CONSTASCII_USTRINGPARAM("component context fails to supply service ooo.vba.excel.Window of type ooo.vba.excel.XWindow")), the_context);
        }
        return the_instance;
    }

private:
    Window(); // not implemented
    Window(Window &); // not implemented
    ~Window(); // not implemented
    void operator =(Window); // not implemented
};

} } }

#endif // INCLUDED_OOO_VBA_EXCEL_WINDOW_HPP

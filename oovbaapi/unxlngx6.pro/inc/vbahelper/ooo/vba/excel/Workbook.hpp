#ifndef INCLUDED_OOO_VBA_EXCEL_WORKBOOK_HPP
#define INCLUDED_OOO_VBA_EXCEL_WORKBOOK_HPP

#include "sal/config.h"

#include "ooo/vba/XHelperInterface.hpp"
#include "ooo/vba/excel/XWorkbook.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/lang/XMultiComponentFactory.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/frame/XModel.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace ooo { namespace vba { namespace excel {

class Workbook {
public:
    static ::com::sun::star::uno::Reference< ::ooo::vba::excel::XWorkbook > createWorkbook(::com::sun::star::uno::Reference< ::com::sun::star::uno::XComponentContext > const & the_context, const ::com::sun::star::uno::Reference< ::ooo::vba::XHelperInterface >& Parent, const ::com::sun::star::uno::Reference< ::com::sun::star::frame::XModel >& xModel) {
        ::com::sun::star::uno::Reference< ::com::sun::star::lang::XMultiComponentFactory > the_factory(the_context->getServiceManager());
        if (!the_factory.is()) {
            throw com::sun::star::uno::DeploymentException(::rtl::OUString(RTL_CONSTASCII_USTRINGPARAM("component context fails to supply service manager")), the_context);
        }
        ::com::sun::star::uno::Sequence< ::com::sun::star::uno::Any > the_arguments(2);
        the_arguments[0] <<= Parent;
        the_arguments[1] <<= xModel;
        ::com::sun::star::uno::Reference< ::ooo::vba::excel::XWorkbook > the_instance;
        try {
            the_instance = ::com::sun::star::uno::Reference< ::ooo::vba::excel::XWorkbook >(the_factory->createInstanceWithArgumentsAndContext(::rtl::OUString(RTL_CONSTASCII_USTRINGPARAM("ooo.vba.excel.Workbook")), the_arguments, the_context), ::com::sun::star::uno::UNO_QUERY);
        } catch (::com::sun::star::uno::RuntimeException &) {
            throw;
        } catch (::com::sun::star::uno::Exception & the_exception) {
            throw ::com::sun::star::uno::DeploymentException(::rtl::OUString(RTL_CONSTASCII_USTRINGPARAM("component context fails to supply service ooo.vba.excel.Workbook of type ooo.vba.excel.XWorkbook: ")) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::com::sun::star::uno::DeploymentException(::rtl::OUString(RTL_CONSTASCII_USTRINGPARAM("component context fails to supply service ooo.vba.excel.Workbook of type ooo.vba.excel.XWorkbook")), the_context);
        }
        return the_instance;
    }

private:
    Workbook(); // not implemented
    Workbook(Workbook &); // not implemented
    ~Workbook(); // not implemented
    void operator =(Workbook); // not implemented
};

} } }

#endif // INCLUDED_OOO_VBA_EXCEL_WORKBOOK_HPP

#ifndef INCLUDED_OOO_VBA_EXCEL_XAXISTITLE_HPP
#define INCLUDED_OOO_VBA_EXCEL_XAXISTITLE_HPP

#include "sal/config.h"

#include "ooo/vba/excel/XAxisTitle.hdl"

#include "ooo/vba/excel/XTitle.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"

namespace ooo { namespace vba { namespace excel {

inline ::com::sun::star::uno::Type const & cppu_detail_getUnoType(::ooo::vba::excel::XAxisTitle const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_TypeDescriptionReference * aSuperTypes[1];
        aSuperTypes[0] = ::cppu::UnoType< const ::com::sun::star::uno::Reference< ::ooo::vba::excel::XTitle > >::get().getTypeLibType();
        typelib_static_mi_interface_type_init( &the_type, "ooo.vba.excel.XAxisTitle", 1, aSuperTypes );
    }
    return * reinterpret_cast< ::com::sun::star::uno::Type * >( &the_type );
}

} } }

inline ::com::sun::star::uno::Type const & SAL_CALL getCppuType(::com::sun::star::uno::Reference< ::ooo::vba::excel::XAxisTitle > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::com::sun::star::uno::Reference< ::ooo::vba::excel::XAxisTitle > >::get();
}

::com::sun::star::uno::Type const & ::ooo::vba::excel::XAxisTitle::static_type(void *) {
    return ::getCppuType(static_cast< ::com::sun::star::uno::Reference< ::ooo::vba::excel::XAxisTitle > * >(0));
}

#endif // INCLUDED_OOO_VBA_EXCEL_XAXISTITLE_HPP

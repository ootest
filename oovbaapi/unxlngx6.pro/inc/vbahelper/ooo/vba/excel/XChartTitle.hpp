#ifndef INCLUDED_OOO_VBA_EXCEL_XCHARTTITLE_HPP
#define INCLUDED_OOO_VBA_EXCEL_XCHARTTITLE_HPP

#include "sal/config.h"

#include "ooo/vba/excel/XChartTitle.hdl"

#include "ooo/vba/excel/XTitle.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"

namespace ooo { namespace vba { namespace excel {

inline ::com::sun::star::uno::Type const & cppu_detail_getUnoType(::ooo::vba::excel::XChartTitle const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_TypeDescriptionReference * aSuperTypes[1];
        aSuperTypes[0] = ::cppu::UnoType< const ::com::sun::star::uno::Reference< ::ooo::vba::excel::XTitle > >::get().getTypeLibType();
        typelib_static_mi_interface_type_init( &the_type, "ooo.vba.excel.XChartTitle", 1, aSuperTypes );
    }
    return * reinterpret_cast< ::com::sun::star::uno::Type * >( &the_type );
}

} } }

inline ::com::sun::star::uno::Type const & SAL_CALL getCppuType(::com::sun::star::uno::Reference< ::ooo::vba::excel::XChartTitle > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::com::sun::star::uno::Reference< ::ooo::vba::excel::XChartTitle > >::get();
}

::com::sun::star::uno::Type const & ::ooo::vba::excel::XChartTitle::static_type(void *) {
    return ::getCppuType(static_cast< ::com::sun::star::uno::Reference< ::ooo::vba::excel::XChartTitle > * >(0));
}

#endif // INCLUDED_OOO_VBA_EXCEL_XCHARTTITLE_HPP

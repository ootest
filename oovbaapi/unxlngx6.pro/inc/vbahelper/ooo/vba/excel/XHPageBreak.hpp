#ifndef INCLUDED_OOO_VBA_EXCEL_XHPAGEBREAK_HPP
#define INCLUDED_OOO_VBA_EXCEL_XHPAGEBREAK_HPP

#include "sal/config.h"

#include "ooo/vba/excel/XHPageBreak.hdl"

#include "ooo/vba/excel/XPageBreak.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"

namespace ooo { namespace vba { namespace excel {

inline ::com::sun::star::uno::Type const & cppu_detail_getUnoType(::ooo::vba::excel::XHPageBreak const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_TypeDescriptionReference * aSuperTypes[1];
        aSuperTypes[0] = ::cppu::UnoType< const ::com::sun::star::uno::Reference< ::ooo::vba::excel::XPageBreak > >::get().getTypeLibType();
        typelib_static_mi_interface_type_init( &the_type, "ooo.vba.excel.XHPageBreak", 1, aSuperTypes );
    }
    return * reinterpret_cast< ::com::sun::star::uno::Type * >( &the_type );
}

} } }

inline ::com::sun::star::uno::Type const & SAL_CALL getCppuType(::com::sun::star::uno::Reference< ::ooo::vba::excel::XHPageBreak > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::com::sun::star::uno::Reference< ::ooo::vba::excel::XHPageBreak > >::get();
}

::com::sun::star::uno::Type const & ::ooo::vba::excel::XHPageBreak::static_type(void *) {
    return ::getCppuType(static_cast< ::com::sun::star::uno::Reference< ::ooo::vba::excel::XHPageBreak > * >(0));
}

#endif // INCLUDED_OOO_VBA_EXCEL_XHPAGEBREAK_HPP

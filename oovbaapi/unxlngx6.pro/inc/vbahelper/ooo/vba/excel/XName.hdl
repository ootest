#ifndef INCLUDED_OOO_VBA_EXCEL_XNAME_HDL
#define INCLUDED_OOO_VBA_EXCEL_XNAME_HDL

#include "sal/config.h"

#include "ooo/vba/XHelperInterface.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
namespace ooo { namespace vba { namespace excel { class XRange; } } }
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno {
class Type;
} } } }

namespace ooo { namespace vba { namespace excel {

class SAL_NO_VTABLE XName : public ::ooo::vba::XHelperInterface
{
public:

    // Attributes
    virtual ::rtl::OUString SAL_CALL getName() throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual void SAL_CALL setName( const ::rtl::OUString& _name ) throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual ::rtl::OUString SAL_CALL getNameLocal() throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual void SAL_CALL setNameLocal( const ::rtl::OUString& _namelocal ) throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual ::sal_Bool SAL_CALL getVisible() throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual void SAL_CALL setVisible( ::sal_Bool _visible ) throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual ::rtl::OUString SAL_CALL getValue() throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual ::rtl::OUString SAL_CALL getRefersTo() throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual ::rtl::OUString SAL_CALL getRefersToLocal() throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual ::rtl::OUString SAL_CALL getRefersToR1C1() throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual ::rtl::OUString SAL_CALL getRefersToR1C1Local() throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual ::com::sun::star::uno::Reference< ::ooo::vba::excel::XRange > SAL_CALL getRefersToRange() throw (::com::sun::star::uno::RuntimeException) = 0;

    // Methods
    virtual void SAL_CALL Delete(  ) throw (::com::sun::star::uno::RuntimeException) = 0;

    static inline ::com::sun::star::uno::Type const & SAL_CALL static_type(void * = 0);
};

} } }

inline const ::com::sun::star::uno::Type& SAL_CALL getCppuType( const ::com::sun::star::uno::Reference< ::ooo::vba::excel::XName >* ) SAL_THROW( () );

#endif // INCLUDED_OOO_VBA_EXCEL_XNAME_HDL

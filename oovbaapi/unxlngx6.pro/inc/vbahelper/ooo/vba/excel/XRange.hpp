#ifndef INCLUDED_OOO_VBA_EXCEL_XRANGE_HPP
#define INCLUDED_OOO_VBA_EXCEL_XRANGE_HPP

#include "sal/config.h"

#include "ooo/vba/excel/XRange.hdl"

#include "ooo/vba/excel/XValidation.hpp"
#include "com/sun/star/script/BasicErrorException.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/script/XDefaultProperty.hpp"
#include "ooo/vba/excel/XComment.hpp"
#include "com/sun/star/script/XDefaultMethod.hpp"
#include "ooo/vba/excel/XRange.hpp"
#include "ooo/vba/excel/XCharacters.hpp"
#include "ooo/vba/excel/XFormat.hpp"
#include "com/sun/star/container/XEnumerationAccess.hpp"
#include "ooo/vba/excel/XWorksheet.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace ooo { namespace vba { namespace excel {

inline ::com::sun::star::uno::Type const & cppu_detail_getUnoType(::ooo::vba::excel::XRange const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_TypeDescriptionReference * aSuperTypes[4];
        aSuperTypes[0] = ::cppu::UnoType< const ::com::sun::star::uno::Reference< ::com::sun::star::container::XEnumerationAccess > >::get().getTypeLibType();
        aSuperTypes[1] = ::cppu::UnoType< const ::com::sun::star::uno::Reference< ::com::sun::star::script::XDefaultMethod > >::get().getTypeLibType();
        aSuperTypes[2] = ::cppu::UnoType< const ::com::sun::star::uno::Reference< ::com::sun::star::script::XDefaultProperty > >::get().getTypeLibType();
        aSuperTypes[3] = ::cppu::UnoType< const ::com::sun::star::uno::Reference< ::ooo::vba::excel::XFormat > >::get().getTypeLibType();
        typelib_static_mi_interface_type_init( &the_type, "ooo.vba.excel.XRange", 4, aSuperTypes );
    }
    return * reinterpret_cast< ::com::sun::star::uno::Type * >( &the_type );
}

} } }

inline ::com::sun::star::uno::Type const & SAL_CALL getCppuType(::com::sun::star::uno::Reference< ::ooo::vba::excel::XRange > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::com::sun::star::uno::Reference< ::ooo::vba::excel::XRange > >::get();
}

::com::sun::star::uno::Type const & ::ooo::vba::excel::XRange::static_type(void *) {
    return ::getCppuType(static_cast< ::com::sun::star::uno::Reference< ::ooo::vba::excel::XRange > * >(0));
}

#endif // INCLUDED_OOO_VBA_EXCEL_XRANGE_HPP

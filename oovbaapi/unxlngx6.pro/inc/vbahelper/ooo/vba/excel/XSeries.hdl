#ifndef INCLUDED_OOO_VBA_EXCEL_XSERIES_HDL
#define INCLUDED_OOO_VBA_EXCEL_XSERIES_HDL

#include "sal/config.h"

#include "ooo/vba/XHelperInterface.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno {
class Type;
} } } }

namespace ooo { namespace vba { namespace excel {

class SAL_NO_VTABLE XSeries : public ::ooo::vba::XHelperInterface
{
public:

    // Attributes
    virtual ::rtl::OUString SAL_CALL getName() throw (::com::sun::star::uno::RuntimeException) = 0;

    // Methods
    virtual ::com::sun::star::uno::Any SAL_CALL Series( const ::com::sun::star::uno::Any& Item ) throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual ::com::sun::star::uno::Any SAL_CALL Values( const ::com::sun::star::uno::Any& Values ) throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual ::com::sun::star::uno::Any SAL_CALL XValues( const ::com::sun::star::uno::Any& XValues ) throw (::com::sun::star::uno::RuntimeException) = 0;

    static inline ::com::sun::star::uno::Type const & SAL_CALL static_type(void * = 0);
};

} } }

inline const ::com::sun::star::uno::Type& SAL_CALL getCppuType( const ::com::sun::star::uno::Reference< ::ooo::vba::excel::XSeries >* ) SAL_THROW( () );

#endif // INCLUDED_OOO_VBA_EXCEL_XSERIES_HDL

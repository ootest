#ifndef INCLUDED_OOO_VBA_EXCEL_XWINDOW_HPP
#define INCLUDED_OOO_VBA_EXCEL_XWINDOW_HPP

#include "sal/config.h"

#include "ooo/vba/excel/XWindow.hdl"

#include "ooo/vba/XHelperInterface.hpp"
#include "com/sun/star/script/BasicErrorException.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "ooo/vba/excel/XPane.hpp"
#include "ooo/vba/excel/XRange.hpp"
#include "ooo/vba/excel/XWorksheet.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "sal/types.h"

namespace ooo { namespace vba { namespace excel {

inline ::com::sun::star::uno::Type const & cppu_detail_getUnoType(::ooo::vba::excel::XWindow const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_TypeDescriptionReference * aSuperTypes[1];
        aSuperTypes[0] = ::cppu::UnoType< const ::com::sun::star::uno::Reference< ::ooo::vba::XHelperInterface > >::get().getTypeLibType();
        typelib_static_mi_interface_type_init( &the_type, "ooo.vba.excel.XWindow", 1, aSuperTypes );
    }
    return * reinterpret_cast< ::com::sun::star::uno::Type * >( &the_type );
}

} } }

inline ::com::sun::star::uno::Type const & SAL_CALL getCppuType(::com::sun::star::uno::Reference< ::ooo::vba::excel::XWindow > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::com::sun::star::uno::Reference< ::ooo::vba::excel::XWindow > >::get();
}

::com::sun::star::uno::Type const & ::ooo::vba::excel::XWindow::static_type(void *) {
    return ::getCppuType(static_cast< ::com::sun::star::uno::Reference< ::ooo::vba::excel::XWindow > * >(0));
}

#endif // INCLUDED_OOO_VBA_EXCEL_XWINDOW_HPP

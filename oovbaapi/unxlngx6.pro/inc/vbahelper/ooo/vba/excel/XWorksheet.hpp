#ifndef INCLUDED_OOO_VBA_EXCEL_XWORKSHEET_HPP
#define INCLUDED_OOO_VBA_EXCEL_XWORKSHEET_HPP

#include "sal/config.h"

#include "ooo/vba/excel/XWorksheet.hdl"

#include "com/sun/star/script/XInvocation.hpp"
#include "ooo/vba/XHelperInterface.hpp"
#include "com/sun/star/script/BasicErrorException.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "ooo/vba/excel/XOutline.hpp"
#include "ooo/vba/excel/XRange.hpp"
#include "ooo/vba/excel/XPageSetup.hpp"
#include "com/sun/star/container/XNamed.hpp"
#include "ooo/vba/excel/XWorksheet.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace ooo { namespace vba { namespace excel {

inline ::com::sun::star::uno::Type const & cppu_detail_getUnoType(::ooo::vba::excel::XWorksheet const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_TypeDescriptionReference * aSuperTypes[3];
        aSuperTypes[0] = ::cppu::UnoType< const ::com::sun::star::uno::Reference< ::ooo::vba::XHelperInterface > >::get().getTypeLibType();
        aSuperTypes[1] = ::cppu::UnoType< const ::com::sun::star::uno::Reference< ::com::sun::star::script::XInvocation > >::get().getTypeLibType();
        aSuperTypes[2] = ::cppu::UnoType< const ::com::sun::star::uno::Reference< ::com::sun::star::container::XNamed > >::get().getTypeLibType();
        typelib_static_mi_interface_type_init( &the_type, "ooo.vba.excel.XWorksheet", 3, aSuperTypes );
    }
    return * reinterpret_cast< ::com::sun::star::uno::Type * >( &the_type );
}

} } }

inline ::com::sun::star::uno::Type const & SAL_CALL getCppuType(::com::sun::star::uno::Reference< ::ooo::vba::excel::XWorksheet > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::com::sun::star::uno::Reference< ::ooo::vba::excel::XWorksheet > >::get();
}

::com::sun::star::uno::Type const & ::ooo::vba::excel::XWorksheet::static_type(void *) {
    return ::getCppuType(static_cast< ::com::sun::star::uno::Reference< ::ooo::vba::excel::XWorksheet > * >(0));
}

#endif // INCLUDED_OOO_VBA_EXCEL_XWORKSHEET_HPP

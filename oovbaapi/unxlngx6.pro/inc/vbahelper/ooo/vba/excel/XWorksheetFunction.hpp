#ifndef INCLUDED_OOO_VBA_EXCEL_XWORKSHEETFUNCTION_HPP
#define INCLUDED_OOO_VBA_EXCEL_XWORKSHEETFUNCTION_HPP

#include "sal/config.h"

#include "ooo/vba/excel/XWorksheetFunction.hdl"

#include "com/sun/star/script/XInvocation.hpp"
#include "ooo/vba/XHelperInterface.hpp"
#include "com/sun/star/beans/XExactName.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"

namespace ooo { namespace vba { namespace excel {

inline ::com::sun::star::uno::Type const & cppu_detail_getUnoType(::ooo::vba::excel::XWorksheetFunction const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_TypeDescriptionReference * aSuperTypes[3];
        aSuperTypes[0] = ::cppu::UnoType< const ::com::sun::star::uno::Reference< ::ooo::vba::XHelperInterface > >::get().getTypeLibType();
        aSuperTypes[1] = ::cppu::UnoType< const ::com::sun::star::uno::Reference< ::com::sun::star::beans::XExactName > >::get().getTypeLibType();
        aSuperTypes[2] = ::cppu::UnoType< const ::com::sun::star::uno::Reference< ::com::sun::star::script::XInvocation > >::get().getTypeLibType();
        typelib_static_mi_interface_type_init( &the_type, "ooo.vba.excel.XWorksheetFunction", 3, aSuperTypes );
    }
    return * reinterpret_cast< ::com::sun::star::uno::Type * >( &the_type );
}

} } }

inline ::com::sun::star::uno::Type const & SAL_CALL getCppuType(::com::sun::star::uno::Reference< ::ooo::vba::excel::XWorksheetFunction > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::com::sun::star::uno::Reference< ::ooo::vba::excel::XWorksheetFunction > >::get();
}

::com::sun::star::uno::Type const & ::ooo::vba::excel::XWorksheetFunction::static_type(void *) {
    return ::getCppuType(static_cast< ::com::sun::star::uno::Reference< ::ooo::vba::excel::XWorksheetFunction > * >(0));
}

#endif // INCLUDED_OOO_VBA_EXCEL_XWORKSHEETFUNCTION_HPP

#ifndef INCLUDED_OOO_VBA_MSFORMS_XPICTUREFORMAT_HDL
#define INCLUDED_OOO_VBA_MSFORMS_XPICTUREFORMAT_HDL

#include "sal/config.h"

#include "ooo/vba/XHelperInterface.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno {
class Type;
} } } }

namespace ooo { namespace vba { namespace msforms {

class SAL_NO_VTABLE XPictureFormat : public ::ooo::vba::XHelperInterface
{
public:

    // Attributes
    virtual double SAL_CALL getBrightness() throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual void SAL_CALL setBrightness( double _brightness ) throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual double SAL_CALL getContrast() throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual void SAL_CALL setContrast( double _contrast ) throw (::com::sun::star::uno::RuntimeException) = 0;

    // Methods
    virtual void SAL_CALL IncrementBrightness( double increment ) throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual void SAL_CALL IncrementContrast( double increment ) throw (::com::sun::star::uno::RuntimeException) = 0;

    static inline ::com::sun::star::uno::Type const & SAL_CALL static_type(void * = 0);
};

} } }

inline const ::com::sun::star::uno::Type& SAL_CALL getCppuType( const ::com::sun::star::uno::Reference< ::ooo::vba::msforms::XPictureFormat >* ) SAL_THROW( () );

#endif // INCLUDED_OOO_VBA_MSFORMS_XPICTUREFORMAT_HDL

#ifndef INCLUDED_OOO_VBA_MSFORMS_XSCROLLBAR_HDL
#define INCLUDED_OOO_VBA_MSFORMS_XSCROLLBAR_HDL

#include "sal/config.h"

#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno {
class Type;
} } } }

namespace ooo { namespace vba { namespace msforms {

class SAL_NO_VTABLE XScrollBar : public ::com::sun::star::uno::XInterface
{
public:

    // Attributes
    virtual ::sal_Int32 SAL_CALL getMax() throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual void SAL_CALL setMax( ::sal_Int32 _max ) throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual ::sal_Int32 SAL_CALL getMin() throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual void SAL_CALL setMin( ::sal_Int32 _min ) throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual ::com::sun::star::uno::Any SAL_CALL getValue() throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual void SAL_CALL setValue( const ::com::sun::star::uno::Any& _value ) throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual ::sal_Int32 SAL_CALL getLargeChange() throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual void SAL_CALL setLargeChange( ::sal_Int32 _largechange ) throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual ::sal_Int32 SAL_CALL getSmallChange() throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual void SAL_CALL setSmallChange( ::sal_Int32 _smallchange ) throw (::com::sun::star::uno::RuntimeException) = 0;

    static inline ::com::sun::star::uno::Type const & SAL_CALL static_type(void * = 0);
};

} } }

inline const ::com::sun::star::uno::Type& SAL_CALL getCppuType( const ::com::sun::star::uno::Reference< ::ooo::vba::msforms::XScrollBar >* ) SAL_THROW( () );

#endif // INCLUDED_OOO_VBA_MSFORMS_XSCROLLBAR_HDL

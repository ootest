#ifndef INCLUDED_OOO_VBA_MSFORMS_XSHAPES_HPP
#define INCLUDED_OOO_VBA_MSFORMS_XSHAPES_HPP

#include "sal/config.h"

#include "ooo/vba/msforms/XShapes.hdl"

#include "ooo/vba/msforms/XShapeRange.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "ooo/vba/XCollection.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "sal/types.h"

namespace ooo { namespace vba { namespace msforms {

inline ::com::sun::star::uno::Type const & cppu_detail_getUnoType(::ooo::vba::msforms::XShapes const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_TypeDescriptionReference * aSuperTypes[1];
        aSuperTypes[0] = ::cppu::UnoType< const ::com::sun::star::uno::Reference< ::ooo::vba::XCollection > >::get().getTypeLibType();
        typelib_static_mi_interface_type_init( &the_type, "ooo.vba.msforms.XShapes", 1, aSuperTypes );
    }
    return * reinterpret_cast< ::com::sun::star::uno::Type * >( &the_type );
}

} } }

inline ::com::sun::star::uno::Type const & SAL_CALL getCppuType(::com::sun::star::uno::Reference< ::ooo::vba::msforms::XShapes > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::com::sun::star::uno::Reference< ::ooo::vba::msforms::XShapes > >::get();
}

::com::sun::star::uno::Type const & ::ooo::vba::msforms::XShapes::static_type(void *) {
    return ::getCppuType(static_cast< ::com::sun::star::uno::Reference< ::ooo::vba::msforms::XShapes > * >(0));
}

#endif // INCLUDED_OOO_VBA_MSFORMS_XSHAPES_HPP

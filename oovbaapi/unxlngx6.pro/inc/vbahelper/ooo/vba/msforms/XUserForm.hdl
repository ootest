#ifndef INCLUDED_OOO_VBA_MSFORMS_XUSERFORM_HDL
#define INCLUDED_OOO_VBA_MSFORMS_XUSERFORM_HDL

#include "sal/config.h"

#include "com/sun/star/script/XInvocation.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno {
class Type;
} } } }

namespace ooo { namespace vba { namespace msforms {

class SAL_NO_VTABLE XUserForm : public ::com::sun::star::script::XInvocation
{
public:

    // Attributes
    virtual ::rtl::OUString SAL_CALL getCaption() throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual void SAL_CALL setCaption( const ::rtl::OUString& _caption ) throw (::com::sun::star::uno::RuntimeException) = 0;

    // Methods
    virtual void SAL_CALL Show(  ) throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual void SAL_CALL Hide(  ) throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual void SAL_CALL RePaint(  ) throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual void SAL_CALL UnloadObject(  ) throw (::com::sun::star::uno::RuntimeException) = 0;
    virtual ::com::sun::star::uno::Any SAL_CALL Controls( const ::com::sun::star::uno::Any& index ) throw (::com::sun::star::uno::RuntimeException) = 0;

    static inline ::com::sun::star::uno::Type const & SAL_CALL static_type(void * = 0);
};

} } }

inline const ::com::sun::star::uno::Type& SAL_CALL getCppuType( const ::com::sun::star::uno::Reference< ::ooo::vba::msforms::XUserForm >* ) SAL_THROW( () );

#endif // INCLUDED_OOO_VBA_MSFORMS_XUSERFORM_HDL
